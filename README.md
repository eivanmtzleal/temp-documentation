# USCIS ACTIONS - AOS

## [USCIS Forms - Missing Flows Changes Document- link](https://docs.google.com/document/d/1W3CBYbgqZ8FRJ7oxgN62PE1DlhcQaFNVTmMVcHE9kLM/edit)

(https://docs.google.com/document/d/1W3CBYbgqZ8FRJ7oxgN62PE1DlhcQaFNVTmMVcHE9kLM/edit)

## [ I-140 - ACTIONS ] — [ AOS ]

### 0.- [ I-140 - CREATE ] [OK]

- [x] Create Main information (I-140 Filed Date[Just required], I-140 Receipt Number[Not Required], i-140 Receipt Copy[Not required], Filing Type[Not Required] )
- [x] EDIT Approval information

### 1.- [ I-140 - APPROVED ] [OK]

- [x] CREATE Approval STATUS (I-140 Approval Date [Just Required], i-140 Approval Letter[Not required])
- [x] EDIT Approval information

### 2.- [ I-140 - DENIED ][ok]

- [x] CREATE DENIED STATUS (I-140 Denial Date [Required], I-140 Denial Letter Copy [Required])
- [x] DENIED Status in the Brief Table
- [x] Appears RE-FILE BTN

### 2-A.- [ I-140 - RE-FILE ][ok]

- [x] CREATE RE-FILE STATUS (I-140 Filed Date[Just required], I-140 Receipt Number[Not Required], i-140 Receipt Copy[Not required], Filing Type[Not Required] )
- [x] ARCHIVE the last I-140 into the ARCHIVE LIST

### 3.- [ I-140 - CREATE REF ][ok]

- [x] CREATE Approved information (I-140 RFE Issued Date[Required], I-140 RFE Due Date[Required], I-140 RFE Response Date[Not required], I-140 RFE Office No.[Not required], I-140 RFE Letter [Not required], I-140 RFE Response Letter [Not required] )
- [x] Info in Brief Table
- [x] WHEN RFE IS CREATED APPER THE: APPROVED BTN and DENIED BTN
- [x] EDIT RFE AUDIT information

### 3-A.- [ I-140 -CREATE REF - APPROVED ][ok]

- [x] CREATE RFE Approval STATUS (I-140 Approval Date [Just Required], i-140 Approval Letter[Not required])
- [x] EDIT RFE Approval information

### 4.- [ CREATE- REF-DENIED ][ok]

- [x] CREATE RFE DENIED STATUS (I-140 RFE Denial Date [Required], I-140 RFE Denial Letter Copy [Required])
- [x] RFE DENIED Status in the Brief Table
- [x] Appears RE-FILE BTN

### 5.-[ I-140 - NOID ][ok]

- [x] CREATE DENIED information (I-140 NOID Issued Date[Required], I-140 NOID Due Date[Required], I-140 NOID Response Date[Not required], I-140 NOID Office No.[Not required], I-140 NOID Letter [Not required], I-140 NOID Response Letter [Not required] )
- [x] WHEN NOID IS CREATED APPEAR THE: APPROVED BTN and DENIED BTN
- [x] Cannot be edited

### 5-A.-[ I-140 - NOID-APPROVED ][ok]

- [x] CREATE NOID Approval STATUS (I-140 NOID Approval Date [Just Required], i-140 NOID Approval Letter[Not required])
- [x] EDIT NOID Approval information

### 6.- [ I-140 - NOID-DENIED ][ok]

- [x] CREATE NOID DENIED STATUS (I-140 NOID Denied Date [Just Required], i-140 NOID Denied Letter Copy[Required])
- [x] NOID Denied status into Brief Table
- [x] Appears Re-File btn

## [ I-765 - ACTIONS ] — [ AOS ]

### 0.- [ CREATE ] [OK]

- [x] Create Main information (I-765 Filed Date[Just required], I-765 Receipt Number[Not Required], i-765 Receipt Copy[Not required])
- [x] EDIT Approval information

### 1.- [ APPROVED ][ok]

- [x] CREATE Approval STATUS (I-765 Approval Date[required], I-765 Approval Letter[No Required], Valid for[Required], I-765 Expiration Date[Required] )
- [x] EDIT Approval information

### 2.- [ DENIED ][ok]

- [x] CREATE DENIED STATUS (I-765 Denial Date [Required], I-765 Denial Letter Copy [Required])
- [x] DENIED In the Brief Table

### 3.- [ CREATE REF ][ok]

- [x] CREATE Approved information (I-765 RFE Issued Date[Required], I-765 RFE Due Date[Required], I-765 RFE Response Date[Not required], I-765 RFE Office No.[Not required], I-765 RFE Letter [Not required], I-765 RFE Response Letter [Not required] )
- [x] WHEN RFE IS CREATED APPER THE: APPROVED BTN and DENIED BTN
- [x] EDIT RFE AUDIT information

### 3-A.- [ I-765 -CREATE REF - APPROVED ][ok]

- [x] CREATE RFE Approval STATUS (I-765 Approval Date [Required], i-765 Approval Letter[Not required], I-765 Valid for [Required], I-765 RFE Expiration Date [Required])
- [x] EDIT RFE Approval information

### 4.- [ CREATE - REF-DENIED ][ok]

- [x] CREATE RFE DENIED STATUS (I-765 RFE Denial Date [Required], I-765 RFE Denial Letter Copy [Required])
- [x] DENIED Status in the Brief Table
- [x] FINISH
- [x] File Cannot be Edited

### 5.- [ CREATE NEW 765 File when it is Approved o Denied ][ok]

(Approved OR Denied push into the history List) ]

### 5-A.- [ CREATE A NEW I765 File When The current I-765 is Approved ]

- [x] Create a New I-765 clicking in the Button action if the current I-765 is APPROVED
- [x] Send the previous I-765 to History List
- [x] Permit create a new I-765 File
- [x] Save the new I-765 File

### 5-B.- [ CREATE A NEW I765 File When The current I-765 is DENIED ][ok]

- [x] Create a New I-765 clicking in the Button action if the current I-765 is DENIED
- [x] Send the previous I-765 to History List
- [x] Permit create a new I-765 File
- [x] Save the new I-765 File

## [ I-131 - ACTIONS ] — [ AOS ]

### 0.- [ CREATE ][ok]

- [x] Create Main information (I-131 Filed Date[Just required], I-131 Receipt Number[Not Required], i-131 Receipt Copy[Not required])
- [x] EDIT Approval information

### 1.- [ APPROVED ][ok]

- [x] CREATE Approval STATUS (I-131 Approval Date[required], I-131 Approval Letter[No Required], Valid for[Required], I-131 Expiration Date[Required] )
- [x] EDIT Approval information

### 2.- [ DENIED ][ok]

- [x] CREATE DENIED STATUS (I-131 Denial Date [Required], I-131 Denial Letter Copy [Required])
- [x] DENIED In the Brief Table

### 3.- [ CERATE RFE ][ok]

- [x] CREATE Approved information (I-131 RFE Issued Date[Required], I-131 RFE Due Date[Required], I-131 RFE Response Date[Not required], I-131 RFE Office No.[Not required], I-131 RFE Letter [Not required], I-131 RFE Response Letter [Not required] )
- [x] WHEN RFE IS CREATED APPEAR THE: APPROVED BTN and DENIED BTN
- [x] EDIT RFE AUDIT information

### 3-A.- [ I-131 -CREATE REF - APPROVED ][ok]

- [x] CREATE RFE Approval STATUS (I-131 Approval Date [Required], i-131 Approval Letter[Not required], I-131 Valid for [Required], I-131 RFE Expiration Date [Required])
- [x] EDIT RFE Approval information

### 4.- [ CREATE- REF-DENIED ][ok]

- [x] CREATE RFE DENIED STATUS (I-131 RFE Denial Date [Required], I-131 RFE Denial Letter Copy [Required])
- [x] DENIED Status in the Brief Table
- [x] FINISH
- [x] File Cannot be Edited

## [ I-485 - ACTIONS ] — [ AOS ]

### 0.- [ CREATE ][ok]

- [x] Create Main information (I-485 Filed Date[Just required], I-485 Receipt Number[Not Required], i-485 Receipt Copy[Not required])
- [x] EDIT Approval information

### 1.- [ APPROVED ][ok]

- [x] CREATE Approval STATUS (I-485 Approval Date [Just Required], i-485 Approval Letter[Not required])
- [x] EDIT Approval information

### 2.- [ DENIED ][ok]

- [x] CREATE DENIED STATUS (I-140 Denial Date [Required], I-140 Denial Letter Copy [Required])
- [x] DENIED Status in the Brief Table
- [x] Appears RE-FILE BTN

### 2-A.- [ I-485 - RE-FILE ][ok]

- [x] CREATE RE-FILE STATUS (I-485 Filed Date[Just required], I-485 Receipt Number[Not Required], i-485 Receipt Copy[Not required])
- [x] ARCHIVE the last I-485 into the ARCHIVE LIST

### 3.- [ CERATE RFE ][ok]

- [x] CREATE Approved information (I-485 RFE Issued Date[Required], I-485 RFE Due Date[Required], I-485 RFE Response Date[Not required], I-485 RFE Office No.[Not required], I-485 RFE Letter [Not required], I-485 RFE Response Letter [Not required] )
- [x] WHEN RFE IS CREATED APPEAR THE: APPROVED BTN and DENIED BTN
- [x] EDIT RFE AUDIT information

### 3-A.- [ I-485 -CREATE REF - APPROVED ][ok]

- [x] CREATE RFE Approval STATUS (I-485 Approval Date [Required], i-485 Approval Letter[Not required])
- [x] EDIT RFE Approval information

### 4.- [ CREATE- REF-DENIED ][ok]

- [x] CREATE RFE DENIED STATUS (I-140 RFE Denial Date [Required], I-140 RFE Denial Letter Copy [Required])
- [x] DENIED Status in the Brief Table
- [x] Appears RE-FILE BTN

### 5.-[ I-485 - NOID ][ok]

- [x] CREATE DENIED information (I-485 NOID Issued Date[Required], I-485 NOID Due Date[Required], I-485 NOID Response Date[Not required], I-485 NOID Office No.[Not required], I-485 NOID Letter [Not required], I-485 NOID Response Letter [Not required] )
- [x] WHEN NOID IS CREATED APPEAR THE: APPROVED BTN and DENIED BTN
- [x] Cannot be edited

### 5-A.-[ I-485 - NOID-APPROVED ][ok]

- [x] CREATE NOID Approval STATUS (I-485 NOID Approval Date [Just Required], i-485 NOID Approval Letter[Not required])
- [x] EDIT NOID Approval information

### 6.- [ I-485 - NOID-DENIED ][ok]

- [x] CREATE NOID DENIED STATUS (I-485 NOID Denied Date [Just Required], i-485 NOID Denied Letter Copy[Required])
- [x] NOID Denied status into Brief Table
- [x] Appears Re-File btn

### 7.- [ INTERVIEWS ]—— [ok]

- [x] EDIT INTERVIEWS
- [x] CREATE INTERVIEWS STATUS

### 8.- [ GREEN CARD FILE ][ok]

- [x] Upload GreenCard File
- [x] Change GreenCard File

## USCIS ACTIONS - CP

## [ I-140 - ACTIONS ] — [ CP ]

### 0.- [ I-140 - CREATE ] [OK]

- [x] Create Main information (I-140 Filed Date[Just required], I-140 Receipt Number[Not Required], i-140 Receipt Copy[Not required], Filing Type[Not Required] )
- [x] EDIT Approval information

### 1.- [ I-140 - APPROVED ] [OK]

- [x] CREATE Approval STATUS (I-140 Approval Date [Just Required], i-140 Approval Letter[Not required])
- [x] EDIT Approval information

### 2.- [ I-140 - DENIED ][ok]

- [x] CREATE DENIED STATUS (I-140 Denial Date [Required], I-140 Denial Letter Copy [Required])
- [x] DENIED Status in the Brief Table
- [x] Appears RE-FILE BTN

### 2-A.- [ I-140 - RE-FILE ][ok]

- [x] CREATE RE-FILE STATUS (I-140 Filed Date[Just required], I-140 Receipt Number[Not Required], i-140 Receipt Copy[Not required], Filing Type[Not Required] )
- [x] ARCHIVE the last I-140 into the ARCHIVE LIST

### 3.- [ I-140 - CREATE REF ][ok]

- [x] CREATE Approved information (I-140 RFE Issued Date[Required], I-140 RFE Due Date[Required], I-140 RFE Response Date[Not required], I-140 RFE Office No.[Not required], I-140 RFE Letter [Not required], I-140 RFE Response Letter [Not required] )
- [x] Info in Brief Table
- [x] WHEN RFE IS CREATED APPEAR THE: APPROVED BTN and DENIED BTN
- [x] EDIT RFE AUDIT information

### 3-A.- [ I-140 -CREATE REF - APPROVED ][ok]

- [x] CREATE RFE Approval STATUS (I-140 Approval Date [Just Required], i-140 Approval Letter[Not required])
- [x] EDIT RFE Approval information

### 4.- [ CREATE- REF-DENIED ][ok]

- [x] CREATE RFE DENIED STATUS (I-140 RFE Denial Date [Required], I-140 RFE Denial Letter Copy [Required])
- [x] RFE DENIED Status in the Brief Table
- [x] Appears RE-FILE BTN

### 5.-[ I-140 - NOID ][ok]

- [x] CREATE DENIED information (I-140 NOID Issued Date[Required], I-140 NOID Due Date[Required], I-140 NOID Response Date[Not required], I-140 NOID Office No.[Not required], I-140 NOID Letter [Not required], I-140 NOID Response Letter [Not required] )
- [x] WHEN NOID IS CREATED APPEAR THE: APPROVED BTN and DENIED BTN
- [x] Cannot be edited

### 5-A.-[ I-140 - NOID-APPROVED ][ok]

- [x] CREATE NOID Approval STATUS (I-140 NOID Approval Date [Just Required], i-140 NOID Approval Letter[Not required])
- [x] EDIT NOID Approval information

### 6.- [ I-140 - NOID-DENIED ][ok]

- [x] CREATE NOID DENIED STATUS (I-140 NOID Denied Date [Just Required], i-140 NOID Denied Letter Copy[Required])
- [x] NOID Denied status into Brief Table
- [x] Appears Re-File btn

# NVC ACTIONS - CP

## [ Main Information - ACTIONS ]

### [ CP - Consular Process Container ][ ok ]

Consular processing container [OK]

- [x] Once I-140 is approved, the system should create a new container below USCIS called “Consular Process - In Progress” with a box in it called “NVC”.

### NVC should have following details

- [x] NVC Fee Notice Date (Req)
- [x] NVC Receipt Number (Opt)
- [x] NVC Receipt Copy (Opt)
- [x] DS-260 Submitted Date (Opt)

## [ Interviews - ACTIONS ] [ CP ]

### 1.- [ CREATE THE INTERVIEW BOX ][ok]

New box should appear below the NVC, called “Interview” with following details:

- [x] Interview Date (Req)
- [x] Interview Time (Opt)
- [x] Location (Opt)
- [x] BDV Letter (Opt)
- [x] ARGO COUPON(Opt)

###Once Saved, we will have Interview Responses:

- [x] 1. Approved button
- [x] 2. AP button
- [x] 3. Denied button
- [x] Save the interview
- [x] Will Appears the Approved Btn, AP, Btn, Denied Btn

### 2.- [ APPROVED - AFTER INTERVIEW IS SAVED ]

### 2-A-[ AP BTN ACTION ][ pending, victor will ask to client about this functionality]

If “AP” is selected, we will just “tag” this applicant as AP (for purposes of USCIS dashboard and visibility). It won’t trigger any additional action as for now and once saved, the response can be “Approved” or “Denied”.

### 2-B- [ APPROVED - AFTER INTERVIEW IS SAVED ]

Approved should have only the “Visa Approved Date” that is required as a box similar to I-485. Visa Approved Date( Required )
Customer Update - PHOTO OF VISA [OPTIONAL]

- [x] Save the Interview CONFIRM APPROVAL[ok]
- [x] Request Re-Upload [ PHOTO VISA][ok]
